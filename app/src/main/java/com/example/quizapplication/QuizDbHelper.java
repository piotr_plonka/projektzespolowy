package com.example.quizapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.quizapplication.QuizContract.*;

import java.util.ArrayList;
import java.util.List;


public class QuizDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "ProjektZespolowy.db";
    private static final int DATABASE_VERSION = 1;

    private static QuizDbHelper instance;

    private SQLiteDatabase db;
    private QuizDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized QuizDbHelper getInstance(Context context){
        if(instance == null){
            instance = new QuizDbHelper(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;

        final String SQL_CREATE_CATEGORIES_TABLE = "CREATE TABLE " +
                CategoriesTable.TABLE_NAME + "( " +
                CategoriesTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CategoriesTable.COLUMN_NAME + " TEXT " +
                ")";

        final String SQL_CREATE_QUESTIONS_TABLE = "CREATE TABLE " +
                QuestionsTable.TABLE_NAME + " ( " +
                QuestionsTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                QuestionsTable.COLUMN_QUESTION + " TEXT, " +
                QuestionsTable.COLUMN_OPTION1 + " TEXT, " +
                QuestionsTable.COLUMN_OPTION2 + " TEXT, " +
                QuestionsTable.COLUMN_OPTION3 + " TEXT, " +
                QuestionsTable.COLUMN_ANSWER_NR + " INTEGER, " +
                QuestionsTable.COLUMN_DIFFICULTY + " TEXT, " +
                QuestionsTable.COLUMN_CATEGORY_ID + " INTEGER, " +
                "FOREIGN KEY(" + QuestionsTable.COLUMN_CATEGORY_ID + ") REFERENCES " +
                CategoriesTable.TABLE_NAME + "(" + CategoriesTable._ID + ")" + "ON DELETE CASCADE" +
                ")";

        db.execSQL(SQL_CREATE_CATEGORIES_TABLE);
        db.execSQL(SQL_CREATE_QUESTIONS_TABLE);
        fillCategoriestable();
        fillQuestionsTable();
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CategoriesTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + QuestionsTable.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    private void fillCategoriestable(){
        Category c1 = new Category("Wszechświat");
        addCategory(c1);
        Category c2 = new Category("Geografia");
        addCategory(c2);
        Category c3 = new Category("Matematyka");
        addCategory(c3);
    }

    private void addCategory(Category category){
        ContentValues cv = new ContentValues();
        cv.put(CategoriesTable.COLUMN_NAME, category.getName());
        db.insert(CategoriesTable.TABLE_NAME, null, cv);
    }

    private void fillQuestionsTable() {
        //        Pytania z kategorii Wszechświat - łatwe
        Question q1 = new Question("Ile jest planet w układzie słonecznym?",
                "7", "8", "9", 2, Question.DIFFICULTY_EASY, Category.COSMOS);
        addQuestion(q1);

        Question q2 = new Question("Czy czarna dziura pochłania wszystko w okół siebie?",
                "Tak", "Nie", "Tylko niektóre gwiazdy", 1, Question.DIFFICULTY_EASY, Category.COSMOS);
        addQuestion(q2);

        Question q3 = new Question("Którą planetę nazywamy Czerwoną planetą?",
                "Venus", "Jowisza", "Marsa", 3, Question.DIFFICULTY_EASY, Category.COSMOS);
        addQuestion(q3);

        //        Pytania z kategorii Wszechświat - średnie

        Question q4 = new Question("Jaka jest ostantia planeta w Układzie Słonecznym?",
                "Neptun", "Saturn", "Uran", 1, Question.DIFFICULTY_MEDIUM, Category.COSMOS);
        addQuestion(q4);

        Question q5 = new Question("Jaka jest najmniejsza planeta w Układzie Słonecznym?",
                "Mars", "Merkury", "Pluton", 2, Question.DIFFICULTY_MEDIUM, Category.COSMOS);
        addQuestion(q5);

        Question q6 = new Question("Jaka jest największa planeta w naszym Układzie Słonecznym?",
                "Saturn", "Jowisz", "Słońce", 2, Question.DIFFICULTY_MEDIUM, Category.COSMOS);
        addQuestion(q6);

        //        Pytania z kategorii Wszechświat - trudne

        Question q7 = new Question("Czy Merkury ma ksieżyce?",
                "Tak - 12", "Tak - 1", "Nie ma żadnego", 3, Question.DIFFICULTY_HARD, Category.COSMOS);
        addQuestion(q7);

        Question q8 = new Question("W ile minut leci światło z Słońca na Ziemię?",
                "12", "9", "8", 3, Question.DIFFICULTY_HARD, Category.COSMOS);
        addQuestion(q8);

        Question q9 = new Question("Księżycem której planety jest Tytan",
                "Jowisza", "Saturna", "Neptuna", 2, Question.DIFFICULTY_HARD, Category.COSMOS);
        addQuestion(q9);

        //        Pytania z kategorii geografia - łatwe

        Question q10 = new Question("Z iloma krajami graniczy Polska?",
                "7", "8", "9", 1, Question.DIFFICULTY_EASY, Category.GEOGRAPHY);
        addQuestion(q10);

        Question q11 = new Question("Najwyższy szczyt Tatr to...",
                "Rysy", "Bystra", "Gerlach", 1, Question.DIFFICULTY_EASY, Category.GEOGRAPHY);
        addQuestion(q11);

        Question q12 = new Question("Jak nazywa się największe polskie jezioro?",
                "Mamry", "Hańcza", "Śniardwy", 3, Question.DIFFICULTY_EASY, Category.GEOGRAPHY);
        addQuestion(q12);

        //        Pytania z kategorii geografia - średnie

        Question q13 = new Question("Stolicą Hiszpanii nie jest...",
                "Sewilla", "Barcelona", "Madryt", 3, Question.DIFFICULTY_MEDIUM, Category.GEOGRAPHY);
        addQuestion(q13);

        Question q14 = new Question("Który z wymienionych krajów ma największą powierzchnię?",
                "Chiny", "Kanada", "Rosja", 3, Question.DIFFICULTY_MEDIUM, Category.GEOGRAPHY);
        addQuestion(q14);

        Question q15 = new Question("Z ilu stanów składają się Stany Zjednoczone?",
                "50", "51", "52", 1, Question.DIFFICULTY_MEDIUM, Category.GEOGRAPHY);
        addQuestion(q15);

        //        Pytania z kategorii geografia - trudne

        Question q16 = new Question("Łza Indii- który kraj ma taki przydomek?",
                "Kambodża", "Madagaskar", "Sri Lanka", 3, Question.DIFFICULTY_HARD, Category.GEOGRAPHY);
        addQuestion(q16);

        Question q17 = new Question("Wskaż kraj, który nie jest monarchią.",
                "Tajlandia", "Portugalia", "Arabia Saudyjska", 2, Question.DIFFICULTY_HARD, Category.GEOGRAPHY);
        addQuestion(q17);

        Question q18 = new Question("Do jakiego państwa należy Zanzibar?",
                "Egiptu", "RPA", "Tanzanii", 3, Question.DIFFICULTY_HARD, Category.GEOGRAPHY);
        addQuestion(q18);

        //        Pytania z kategorii matematyka - łatwe

        Question q19 = new Question("3 do sześcianu to",
                "81", "9", "27", 3, Question.DIFFICULTY_EASY, Category.MATH);
        addQuestion(q19);

        Question q20 = new Question("2 + 4 * 3 = ",
                "18", "14", "24", 2, Question.DIFFICULTY_EASY, Category.MATH);
        addQuestion(q20);

        Question q21 = new Question("9 * 99 = ",
                "891", "981", "889", 1, Question.DIFFICULTY_EASY, Category.MATH);
        addQuestion(q21);

        //        Pytania z kategorii matematyka - średnie

        Question q22 = new Question("Czy kwadrat jest trapezem",
                "Tak", "Nie", "Nie każdy", 1, Question.DIFFICULTY_MEDIUM, Category.MATH);
        addQuestion(q22);

        Question q23 = new Question("Kij miał 4 metry długości. Odcięto 5/8 kija. Pozostała część ma:",
                "1.5m", "3m", "2.5m", 1, Question.DIFFICULTY_MEDIUM, Category.MATH);
        addQuestion(q23);

        Question q24 = new Question("Ile wynosi suma wszystkich liczb od 0 do 100?",
                "999", "5050", "1001", 2, Question.DIFFICULTY_MEDIUM, Category.MATH);
        addQuestion(q24);

        //        Pytania z kategorii matematyka - trudne

        Question q25 = new Question("Najmniejsza liczba pierwsza to",
                "11", "2", "1", 3, Question.DIFFICULTY_HARD, Category.MATH);
        addQuestion(q25);

        Question q26 = new Question("Ołówek i długopis kosztują razem 1,10zł. Długopis jest droższy od ołówka o 1 zł. Ile kosztuje ołówek?",
                "5 groszy", "10 groszy", "15 groszy", 1, Question.DIFFICULTY_HARD, Category.MATH);
        addQuestion(q26);

        Question q27 = new Question("Jeżeli kąt wpisany oparty jest na średnicy koła, to jego miara wynosi",
                "45 stopni", "60 stopni", "90 stopni", 3, Question.DIFFICULTY_HARD, Category.MATH);
        addQuestion(q27);
    }
    private void addQuestion(Question question) {
        ContentValues cv = new ContentValues();
        cv.put(QuestionsTable.COLUMN_QUESTION, question.getQuestion());
        cv.put(QuestionsTable.COLUMN_OPTION1, question.getOption1());
        cv.put(QuestionsTable.COLUMN_OPTION2, question.getOption2());
        cv.put(QuestionsTable.COLUMN_OPTION3, question.getOption3());
        cv.put(QuestionsTable.COLUMN_ANSWER_NR, question.getAnswerNr());
        cv.put(QuestionsTable.COLUMN_DIFFICULTY, question.getDifficulty());
        cv.put(QuestionsTable.COLUMN_CATEGORY_ID, question.getCategoryID());
        db.insert(QuestionsTable.TABLE_NAME, null, cv);
    }

    public List<Category> getAllCategories() {
        List<Category> categoryList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + CategoriesTable.TABLE_NAME, null);

        if (c.moveToFirst()) {
            do{
                Category category = new Category();
                category.setId(c.getInt(c.getColumnIndex(CategoriesTable._ID)));
                category.setName(c.getString(c.getColumnIndex(CategoriesTable.COLUMN_NAME)));
                categoryList.add(category);
            }while(c.moveToNext());
        }

        c.close();
        return categoryList;
    }

    public ArrayList<Question> getAllQuestions() {
        ArrayList<Question> questionList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + QuestionsTable.TABLE_NAME, null);
        if (c.moveToFirst()) {
            do {
                Question question = new Question();
                question.setId(c.getInt(c.getColumnIndex(QuestionsTable._ID)));
                question.setQuestion(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_QUESTION)));
                question.setOption1(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION1)));
                question.setOption2(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION2)));
                question.setOption3(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION3)));
                question.setAnswerNr(c.getInt(c.getColumnIndex(QuestionsTable.COLUMN_ANSWER_NR)));
                question.setDifficulty(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_DIFFICULTY)));
                question.setCategoryID(c.getInt(c.getColumnIndex(QuestionsTable.COLUMN_CATEGORY_ID)));
                questionList.add(question);
            } while (c.moveToNext());
        }
        c.close();
        return questionList;
    }
    public ArrayList<Question> getQuestions(int categoryID,String difficulty) {
        ArrayList<Question> questionList = new ArrayList<>();
        db = getReadableDatabase();

        String selection = QuestionsTable.COLUMN_CATEGORY_ID +  " = ? " +
                " AND " + QuestionsTable.COLUMN_DIFFICULTY + " = ? ";

        String[] selectionArgs = new String[]{String.valueOf(categoryID), difficulty};

        Cursor c = db.query(QuestionsTable.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (c.moveToFirst()) {
            do {
                Question question = new Question();
                question.setId(c.getInt(c.getColumnIndex(QuestionsTable._ID)));
                question.setQuestion(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_QUESTION)));
                question.setOption1(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION1)));
                question.setOption2(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION2)));
                question.setOption3(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION3)));
                question.setAnswerNr(c.getInt(c.getColumnIndex(QuestionsTable.COLUMN_ANSWER_NR)));
                question.setDifficulty(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_DIFFICULTY)));
                question.setCategoryID(c.getInt(c.getColumnIndex(QuestionsTable.COLUMN_CATEGORY_ID)));
                questionList.add(question);
            } while (c.moveToNext());
        }
        c.close();
        return questionList;
    }
}