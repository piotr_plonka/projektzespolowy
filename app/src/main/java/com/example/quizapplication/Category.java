package com.example.quizapplication;

import androidx.annotation.NonNull;

public class Category {
    public static final int COSMOS = 1;
    public static final int GEOGRAPHY = 2;
    public static final int MATH = 3;
    private int id;
    private String name;

    public Category(){}

    public Category(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
